export const TABLE_TYPE_DEFAULT = 'default';
export const TABLE_TYPE_FREE = 'free';
export const TABLE_TYPE_TRIAL = 'trial';

export const DAYS_FOR_RENEWAL = 15;

export const PLAN_TITLE_TRIAL_TEXT = ' Trial';
